#include <stdio.h>
#include <iostream>
#include <string>
#include <windows.h>
const int devidepoint = 2097152;//空白區域
using namespace std;
int trans2( char *arr){//4個BYTE的CHAR ARR 轉回INT
    int sum=0,k;
    for(int i=0;i<4;i++){
        k = arr[i];
		//signed char:-128~127 , convert it to unsigned
        if(k<0){
            k=256+k;
        }
        for(int j=0;j<i;j++){
            k*=256;
        }
        sum+=k;
    }
    return sum;
}
const char *GetEnding(LPCSTR FileName)//取檔案副檔名
{
    string tmp=FileName, ret;
    int o=tmp.find_last_of(".");
    for (int i=o;i<tmp.length();i++)
    {
        ret+=tmp[i];
    }
    return ret.c_str();
}
int main(int argc, char *argv[])
{


    //開啟自己，argv[0]為
    FILE *fp = fopen(argv[0], "rb");
    if (!fp){return 0;}

    fseek(fp, 0, SEEK_END);//fp緩衝區的指標移到最後面
    unsigned long size = ftell(fp);//傳回fp位置(即檔案大小)
    char *buffer = (char*)malloc(size);//宣告此檔案大小的char array
    //將資料讀到buffer
    rewind(fp);
    fread(buffer, size, 1, fp);
    fclose(fp);
    //output的檔案格式:openall.exeXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX|fileinfo|filecontent|fileinfo|filecontent|........
    //i為char(byte)計數器
    //跳至第一個或下一個檔案開頭位置
    int i=devidepoint;

    while(i<size){
        char path[2][MAX_PATH];
        GetCurrentDirectory(MAX_PATH,path[0]);
        GetTempPath(MAX_PATH,path[1]);
        //此檔案的filesize(前四BYTE)
        int filesize = trans2(buffer+i);
        i=i+4;
        //ptr跳至此檔案的filename(以\0為結尾)
        int front=i;
        while(buffer[i]!='\0'){
            i++;
        }
        int namesize = i-front;
        char *filename = new char[namesize];
        filename = buffer+front;
        i++;
        FILE *fp_di;


        bool isexe = true;
        //如非執行檔
        if(strcmp(GetEnding(filename), ".exe")!=0 &&strcmp(GetEnding(filename), ".bat")!=0 ) {
            //路徑建立在DstFolder，並將isexe設為FALSE(此為資料檔)
            isexe = false;
            strcat(path[0],"\\");
            strcat(path[0],filename);
            fp_di = fopen(path[0],"wb");
        }
		//執行檔
        else{
            strcat(path[1],"\\");
            strcat(path[1],filename);
            fp_di = fopen(path[1],"wb");
        }

        //i ptr後面的檔案資料搬到content，並寫入目的檔案
        fwrite(buffer+i,filesize,1,fp_di);
        fclose(fp_di);
        //執行
        if(isexe){
            ShellExecute(NULL,NULL, path[1], NULL, NULL, SW_SHOW);
        }
        //跳至下一個檔案資訊位置
        i+=filesize;
    }
    system("pause");
    return 0;
}
