#include <Windows.h>
#include <stdio.h>
#include <iostream>
#include <string.h>
//let hostfile momory be 2M
const int emptyarea = 1024*1024*2;
using namespace std ;
//轉換int數值成char (1byte)
char* trans(int size){
    char *arr = new char[4];
	for(int i=0;i<4;i++){
        char b = ( char) size;
		arr[i] = b;
		size /=256;
	}
	return arr;
}
int main(int argc, char *argv[]){

	//設定參數，變數之初始值
	string hostfileName,srcName,dstName,srcDir,dstDir,fileName;
	srcName = argv[1];
	dstName = argv[2];
	hostfileName = argv[3];
	srcDir = srcName+"\\*.*";	//來源資料夾的路徑，並設定搜尋方式為全部檔案(*.*)

	///////////////////

	string copyPath = dstName+"\\"+hostfileName;//設定目標檔案的相對路徑
	string openallpath = "Openall.exe";
	//複製副程式openall到目標資料夾
	CopyFile(openallpath.c_str(),copyPath.c_str(),true);
	//開啟資料夾，並設定屬性成可將資料附加在該二進位檔之後
	FILE *fp = fopen(copyPath.c_str(),"ab");
	//fp指標設在緩衝區裡資料最後
    fseek(fp,0,SEEK_END);
	//讀取數值(檔案大小)
    unsigned long subprosize = ftell(fp);
	//插入空白區域到副程式後面，使其達到2MB
    char *onecharPTR = new char[emptyarea-subprosize];
    onecharPTR[0] = '\0';
    fwrite(onecharPTR,emptyarea-subprosize,1,fp);
	//////////////////////
	//找檔案直到iterate完來源資料夾
	WIN32_FIND_DATA fileData;
	HANDLE hList ;
	hList = FindFirstFile(srcDir.c_str(),&fileData);	//偵測路徑位置是否正確
	if(hList == INVALID_HANDLE_VALUE){ //路徑錯誤時中斷程式
		cout <<"error path"<<endl;
		return -1 ;
	}
	//////////////////////
	//HOSTFILE內容格式:Openall.exe |空白區域|1st_info(size+filename)|1st_content|2nd......
	do{
		//open file
		string SfilePath = srcName +"\\"+fileData.cFileName;	//來源檔案的路徑_檔案名
		string filename = fileData.cFileName;
		FILE *fpsrc = fopen(SfilePath.c_str(),"rb+");
		if (fpsrc){
			//get size
            fseek(fpsrc, 0, SEEK_END);
            unsigned long size = ftell(fpsrc);
			//convert size and store it
            char *sizeint = trans(size);
            fwrite(sizeint,4,1,fp);
            //store file name
            const char *filenamearr = filename.c_str();
            fwrite(filenamearr,filename.size()+1,1,fp);
			//get file's content(get the initial index first) and write into hostfile
            rewind(fpsrc);
            char *buffer = (char*)malloc(size);
            fread(buffer, size, 1, fpsrc);
            fwrite(buffer, size, 1, fp);
            fclose(fpsrc);
		}
		else{
			//open file error
            cout<< "problem opeing file" <<"\t"<<fileData.cFileName<<endl;
		}

	}while(FindNextFile(hList,&fileData));

	if(GetLastError()==ERROR_NO_MORE_FILES){ //執行結束時
		cout<<"Complete!"<<endl;
	}
	fclose(fp);
	return 0 ;
}


